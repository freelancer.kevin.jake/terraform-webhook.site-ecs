terraform {
  backend "remote" {
    hostname     = "api.terraform.io"
    organization = "jek-test"

    workspaces {
      prefix = "ps-wordpress-ecs-"
    }
  }
}
